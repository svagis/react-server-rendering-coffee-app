# Created by Jan Svager <jan@svager.cz> on 30. 3. 2014.
"use strict"

express     = require('express')
path        = require('path')
browserify  = require('connect-browserify')
React       = require('react')
nano        = require('nano')
App         = require('./client.coffee')
config      = require('./package.json')
login       = require('./login.json')

# Authenticator
Authenticator = express.basicAuth login.name, login.pass

# App init
app = express()
db = nano('http://'+ login.name + ':' + login.pass + '@46.28.108.112:5984/slenderfit')

# development only
app.configure 'development', ->
  app.use express.logger('dev'), null
  app.use express.static(path.join(__dirname, 'static')), null
  app.use express.errorHandler(), null
  app.get '/bundle.js', browserify
    entry: './client.coffee'
    debug: true
    watch: true

# production only
app.configure 'production', ->
  app.use express.logger('production'), null
  app.use express.compress(), null

# all environments
app.get "/", Authenticator, (req, res) ->
  res.send React.renderComponentToString(App())
app.set 'port', config.port || process.env.PORT

# Listen
app.listen app.get('port'), ->
  console.warn "Express server listening on port #{app.get('port')} in #{app.get 'env'}"

