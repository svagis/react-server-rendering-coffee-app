"use strict"

React = require('react')
ReactMount = require('react/lib/ReactMount')
MainPage = require('./src/index.coffee')
ReactMount.allowFullPageRender = true
R = React.DOM

module.exports = React.createClass
  render: ->
    R.html null,
      R.head null,
        R.link(rel: "stylesheet", href: "style.css"),
        R.script(src: "bundle.js"),
      R.body className: "app",
        MainPage()